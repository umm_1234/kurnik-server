import time
import random

from flask import Flask, session, request
from flask_session import Session

app = Flask(__name__)
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)
  
@app.route("/")
def hello():
    print('Session ID: ' + session.sid)
    return "<h1>Hello world!</h1>"


@app.route("/doors", methods=['GET', 'POST'])
def open_close_doors():
    print('Session ID: ' + session.sid)
    if not "door_text" in session:
        session["door_text"] = "Zavřít"

    if request.method == 'POST':
        session["door_text"] = request.form.get('doors_open')
        return { "doors_open": session["door_text"] }
    else:
        return { "doors_open": session["door_text"] }


@app.route("/info")
def get_henhouse_info():
    print('Session ID: ' + session.sid)
    dictionary = {
            "number_of_eggs": random.randint(1, 8),
            "food_progress": 67,
            "water_progress": 71,
            "leader_name": "Žaneta",
    }
    return dictionary

if __name__ == '__main__':
    app.run()

