# Můj kurník's Flask server

To start the server, make sure you have all the dependencies from
`requirements.txt` installed and run following commands

```bash
export FLASK_APP=muj-kurnik
flask run --cert=<server-certificate> --key=<server-key> --host=0.0.0.0 -p <port>
```

If you don't want the server to accept connections from other IP addresses than
from your computer, don't use the `--host` flag. Host flag makes sure your
server accepts connections from all IP addresses. Through `--cert` and `--key`
server should receive paths to server certificate and its key. Make sure that
the certificate is signed by a Certification Authority that the client device
recognizes (or is included it the client app itself). Here's [a great
guide](https://stackoverflow.com/questions/21297139/how-do-you-sign-a-certificate-signing-request-with-your-certification-authority/21340898#21340898)
describing the whole process of creating a self-signed certificate.

# Development

To create a list of dependencies, run

```python
pip freeze > requirements.txt
```
